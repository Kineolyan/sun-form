import React, { Component } from 'react';
import _ from 'lodash';
import logo from './logo.svg';
import './App.css';

import JoueurReader from './data/JoueurReader';

import Start from './form/Start'
import License from './form/Licence';
import Details from './form/Details';
import Mutuelle from './form/Mutuelle';
import JeunePanel from './form/Jeune';
import SummaryPanel from './form/Summary';
import {
  Step as MuiStep,
  Stepper as MuiStepper,
  StepButton as MuiStepButton
} from 'material-ui/Stepper';

import ModeEditIcon from 'material-ui/svg-icons/editor/mode-edit';

const Step = {
  START: 1,
  DETAILS: 2,
  JEUNE: 3,
  LICENSE: 4,
  MUTUELLE: 5,
  RECAP: 10
};
const STEP_TITLES = {
  [Step.START]: 'Nom du joueur',
  [Step.LICENSE]: 'Type de licence',
  [Step.DETAILS]: 'Information sur le joueur',
  [Step.JEUNE]: 'Jeune joueur (opt.)',
  [Step.MUTUELLE]: 'Choix de mutuelle',
  [Step.RECAP]: 'Récapitulatif'
}
const STEP_ORDER = _(Step)
  .sortBy(_.identity)
  .value();

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      /* Default state
      fillStep: Step.START,
      model: {
        details: {},
        license: {},
        referent: {}
      }
      //*/

      //* Debug state
      fillStep: Step.RECAP,
      reviewStep: Step.RECAP,
      model: {
        "details":{
          "firstName":"Olivier",
          "lastName":"Peyrusse",
          "birthDate":Date.UTC(2000, 0, 1, 3, 0, 0),
          "birthPlace":"Suresnes",
          "address":"23 rue Daumesnil, 94300 VINCENNES",
          "email":"o.peyrusse@gmail.com",
          "phoneNumber":"0685444277",
          "sexe":"homme"
        },
        "license":{
          "type":"Joueur"
        },
        referent:{
          "lastName":"Peyrusse","firstName":"Olivier","phoneNumber":"0987654321"
        },
        "mutuelle":"RC-IDC"
      }
      //*/
    };
  }

  setModel(path, value) {
    const model = this.state.model;
    if (_.isObject(value)) {
      _.merge(_.get(model, path), value);
    } else {
      _.set(this.state.model, path, value);
    }

    this.setState({model});
  }

  saveStart(info) {
    this.setModel('details', info);
    this.continueInForm();
  }

  saveLicense(license) {
    this.setModel('license', license);
    this.continueInForm();
  }

  saveDetails(info) {
    this.setModel('details', info);
    this.continueInForm();
  }

  saveJeune(info) {
    this.setModel('referent', info);
    this.continueInForm();
  }

  saveMutuelle(mutuelle) {
    this.setModel('mutuelle', mutuelle);
    this.continueInForm();
  }

  get currentStep() {
    return this.state.reviewStep || this.state.fillStep;
  }

  isFillComplete() {
    return this.state.fillStep === _.last(STEP_ORDER);
  }

  setStep(step) {
    if (step > this.state.fillStep) {
      this.setState({fillStep: step, reviewStep: undefined});
    } else {
      this.setState({reviewStep: step});
    }
  }

  continueInForm() {
    // So far, always get next step
    let currentStep = this.currentStep;
    let ageFilter = () => true
    if (currentStep >= Step.DETAILS) {
      const age = JoueurReader.getAge(this.state.model.details);
      if (age >= 18) {
        ageFilter = step => step !== Step.JEUNE;
      }
    }

    const nextStep = _(Step)
      .filter(step => step > currentStep)
      .filter(ageFilter)
      .sortBy(_.identity)
      .head();
    this.setStep(nextStep);
  }

  renderStep(step) {
    switch (step) {
      case Step.START: return (
        <Start model={this.state.model.details}
          onSubmit={this.saveStart.bind(this)} />
      );
      case Step.LICENSE: return (
        <License license={this.state.model.license}
          details={this.state.model.details}
          onSubmit={this.saveLicense.bind(this)} />
      );
      case Step.DETAILS: return (
        <Details model={this.state.model.details}
          onSubmit={this.saveDetails.bind(this)} />
      );
      case Step.JEUNE: return (
        <JeunePanel model={this.state.model.referent}
          details={this.state.model.details}
          onSubmit={this.saveJeune.bind(this)} />
      );
      case Step.MUTUELLE: return (
        <Mutuelle mutuelle={this.state.model.mutuelle}
          details={this.state.model.details}
          licence={this.state.model.license}
          onSubmit={this.saveMutuelle.bind(this)} />
      );
      case Step.RECAP: return (
        <SummaryPanel model={this.state.model} />
      );
      default: return (
        <p>Unknown step: {step}</p>
      );
    }
  }

  renderForm() {
    return <div className='sun-form'>
      <div className="form-stepper">
        <MuiStepper
            activeStep={STEP_ORDER.indexOf(this.currentStep)}
            linear={!this.isFillComplete()}
            orientation="vertical">
          {STEP_ORDER.map(step => {
            const formCompleted = this.isFillComplete();
            const stepProps = {
              completed: step < this.state.fillStep || formCompleted
            };
            const buttonProps = {};
            stepProps.disabled = this.state.fillStep > Step.JEUNE
              && step === Step.JEUNE
              && !JoueurReader.isYoung(this.state.model.details);
            if (stepProps.completed || step === this.state.fillStep) {
              buttonProps.onTouchTap = () => this.setStep(step);
            }
            if (!formCompleted && step === this.state.fillStep && step !== this.currentStep) {
              buttonProps.icon = <ModeEditIcon color="#ffb210" />;
            }

            return <MuiStep key={step} {...stepProps}>
              <MuiStepButton {...buttonProps}>
                {STEP_TITLES[step]}
              </MuiStepButton>
            </MuiStep>;
          })}
        </MuiStepper>
      </div>
      <div className="form-content">
        {this.renderStep(this.currentStep)}
      </div>
    </div>;
  }

  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Welcome to React</h2>
        </div>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
        {this.renderForm()}
      </div>
    );
  }
}

export default App;
