import React from 'react';

import JoueurReader from '../data/JoueurReader';
import MUTUELLES from '../data/Mutuelles';

import Paper from 'material-ui/Paper';
import Chip from 'material-ui/Chip';

class SummaryPanel extends React.Component {
  get details() {
    return this.props.model.details;
  }

  get referent() {
    return this.props.model.referent;
  }

  get license() {
    return this.props.model.license;
  }

  get mutuelle() {
    return MUTUELLES[this.props.model.mutuelle];
  }

  renderReferent() {
    return <p>
      Responsable légal: {this.referent.firstName} {this.referent.lastName}, joignable au {this.referent.phoneNumber}
    </p>;
  }

  renderDetails() {
    const birthDate = new Date(this.details.birthDate);
    const young = JoueurReader.isYoung(this.details);

    return <div>
      <Paper className="player-header">
        Joueur : {this.details.firstName} {this.details.lastName}
      </Paper>
      <p>
        Né{JoueurReader.isMale(this.details) ? '' : 'e'} le {birthDate.toLocaleDateString()} à {this.details.birthPlace}
      </p>
      <p>
        Habite à {this.details.address}<br/>
        Joignable au {this.details.phoneNumber} ou par mail sur {this.details.email}.
      </p>
      {young ? this.renderReferent() : null}
      <p>
        {young ? null : <span>Je suis {this.details.job}.<br/></span>}
        Club de l'année précédente : {this.detailsclubBefore || 'Sun'}
      </p>
    </div>;
  }

  renderLicenseAndMore() {
    const cost = JoueurReader.computeCost(this.props.model);
    const mutuelle = this.mutuelle;

    return <div>
      <div>
        Coût de la licence: <Chip style={{display: 'inline-block'}}>{JoueurReader.formatCost(cost)}</Chip>
      </div>
      <p>
        Licence : {this.license.type}<br/>
      Disque du club: {this.license.withDisc ? 'Oui' : 'Pas pour l\'instant'}<br/>
        Mutuelle : {mutuelle.name} {mutuelle.default ? ' (défaut)' : null}
      </p>
    </div>;
  }

  render() {
    return <div>
      {this.renderDetails()}
      {this.renderLicenseAndMore()}
    </div>;
  }
}

SummaryPanel.propTypes = {
  model: React.PropTypes.object.isRequired
}

export default SummaryPanel;
