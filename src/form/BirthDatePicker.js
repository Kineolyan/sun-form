import React from 'react';
import _ from 'lodash';

import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

class BirthDatePicker extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      day: this.props.date.getDate(),
      month: this.props.date.getMonth(),
      year: this.props.date.getFullYear()
    }

    this.cbks = {
      setDay: this.setValue.bind(this, 'day'),
      setMonth: this.setValue.bind(this, 'month'),
      setYear: this.setValue.bind(this, 'year'),
      updateDate: this.updateDate.bind(this)
    };
  }

  setValue(key, event, index, value) {
    this.setState({[key]: value}, this.cbks.updateDate);
  }

  updateDate() {
    this.props.onChange(new Date(this.state.year, this.state.month, this.state.day));
  }

  renderDay() {
    return <SelectField style={{width: 75}} autoWidth={true}
        value={this.state.day}
        onChange={this.cbks.setDay}>
      {_.range(1, 32).map(day => <MenuItem value={day} key={day} primaryText={day} />)}
    </SelectField>;
  }

  renderMonth() {
    return <SelectField style={{width: 175}} autoWidth={true}
        value={this.state.month}
        onChange={this.cbks.setMonth}>
      <MenuItem value={0} primaryText="01 - Janvier" />
      <MenuItem value={1} primaryText="02 - Février" />
      <MenuItem value={2} primaryText="03 - Mars" />
      <MenuItem value={3} primaryText="04 - Avril" />
      <MenuItem value={4} primaryText="05 - Mai" />
      <MenuItem value={5} primaryText="06 - Juin" />
      <MenuItem value={6} primaryText="07 - Juillet" />
      <MenuItem value={7} primaryText="08 - Août" />
      <MenuItem value={8} primaryText="09 - Septembre" />
      <MenuItem value={9} primaryText="10 - Octobre" />
      <MenuItem value={10} primaryText="11 - Novembre" />
      <MenuItem value={11} primaryText="12 - Décembre" />
    </SelectField>;
  }

  renderYear() {
    return <SelectField style={{width: 100}} autoWidth={true}
        value={this.state.year}
        onChange={this.cbks.setYear}>
      {_.range(1950, 2015).map(year => <MenuItem value={year} key={year} primaryText={year} />)}
    </SelectField>;
  }

  render() {
    return <div style={this.props.style}>
      {this.renderDay()} / {this.renderMonth()} / {this.renderYear()}
    </div>;
  }

}

BirthDatePicker.propTypes = {
  date: React.PropTypes.object,
  onChange: React.PropTypes.func.isRequired,
  style: React.PropTypes.object
};

BirthDatePicker.defaultProps = {
  date: new Date(2000, 0, 1),
  style: {}
};

export default BirthDatePicker;
