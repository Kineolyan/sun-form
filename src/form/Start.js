import React from 'react';
import _ from 'lodash';

import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';

class Start extends React.Component {
  constructor(props) {
    super(props);
    this.valueKeys = new Set();
  }

  get model() {
    return this.props.model;
  }

  setValue(key, event, value) {
    this.valueKeys.add(key);
    this.setState({[key]: value});
  }

  submit() {
    this.props.onSubmit(_.pick(this.state, Array.from(this.valueKeys.values())));
  }

  render() {
    return <div style={{textAlign: 'center'}}>
      <div>
        <TextField hintText="Prénom"
          defaultValue={this.model.firstName}
          onChange={this.setValue.bind(this, 'firstName')} />
      </div>
      <div>
        <TextField hintText="Nom"
          defaultValue={this.model.lastName}
          onChange={this.setValue.bind(this, 'lastName')} />
      </div>
      <div>
        <RaisedButton label="Continuer" primary={true} style={{margin: 12}}
          onClick={this.submit.bind(this)}/>
      </div>
    </div>;
  }
}

Start.PropTypes = {
  model: React.PropTypes.object.isRequired,
  onSubmit: React.PropTypes.func.isRequired
};

export default Start;
