import React, {Component} from 'react';
import _ from 'lodash';

import JoueurReader from '../data/JoueurReader';
import BirthDatePicker from './BirthDatePicker';

import TextField from 'material-ui/TextField';
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';
import RaisedButton from 'material-ui/RaisedButton';
import Paper from 'material-ui/Paper';

class DetailsPanel extends Component {

  constructor(props) {
    super(props);
    this.valueKeys = new Set();
    const model = _.assign({}, this.props.model);
    if ('birthDate' in model) {
      model.birthDate = new Date(model.birthDate);
    }

    this.state = model;
  }

  get model() {
    return this.state;
  }

  setModel(key, event, value) {
    this.valueKeys.add(key);
    this.setState({[key]: value});
  }

  isMale() {
    return JoueurReader.isMale(this.state);
  }

  submit() {
    const newModel = _.pick(this.state, Array.from(this.valueKeys.values()));
    if ('birthDate' in newModel) {
      newModel.birthDate = newModel.birthDate.getTime();
    }
    this.props.onSubmit(newModel);
  }

  renderHeader() {
    return <Paper className="player-header">
      Joueur: {this.model.firstName} {this.model.lastName}
    </Paper>;
  }

  renderInput(key, caption, options = {}) {
    return <TextField hintText={caption}
      defaultValue={this.model[key]}
      onChange={this.setModel.bind(this, key)}
      {...options} />
  }

  renderSexe() {
    return <RadioButtonGroup name="sexe" defaultSelected={this.model.sexe}
        onChange={this.setModel.bind(this, 'sexe')}>
      <RadioButton value="homme" label="Homme"/>
      <RadioButton value="femme" label="Femme" />
    </RadioButtonGroup>
  }

  renderBirth() {
    return <div>
      Né{this.isMale() ? '' : 'e'} le
      <BirthDatePicker
        value={this.model.birthDate}
        onChange={value => this.setModel('birthDate', null, value)}
        style={{display: 'inline-block', marginLeft: 5, marginRight: 5}}/>
      à {this.renderInput('birthPlace', 'Lieu de naissance')}
    </div>;
  }

  renderContact() {
    return [
      <div key="address">
        {this.renderInput('address', 'Adresse complète', {multiLine: true})}
      </div>,
      <div key="email">
        {this.renderInput('email', 'Email')}
      </div>,
      <div key="phone">
        {this.renderInput('phoneNumber', 'Téléphone')}
      </div>
    ];
  }

  renderOther() {
    // TODO ne pas demander de profession pour un jeune
    return [
      <div key="profession">
        {this.renderInput('job', 'Profession')}
      </div>,
      <div key="before">
        {this.renderInput('clubBefore', 'Club précédent, si changement')}
      </div>
    ];
  }

  render() {
    return <div>
      {this.renderHeader()}
      {this.renderSexe()}
      {this.renderBirth()}
      {this.renderContact()}
      {this.renderOther()}
      <div>
        <RaisedButton label="Continuer" primary={true} style={{margin: 12}}
          onClick={this.submit.bind(this)}/>
      </div>
      <p className="notice">
        Les informations recueillies sont nécessaires pour l'inscription au Sun.<br/>
      Elles font l'objet d'unt traitement informatique et sont destinées au secrétariat de l'association. Les informations obligatoires comportant une astérisque seront également transmises à la Fédération Flying Disc France (FFDF). En application des articles 39 et suivants de la loi du 8 janvier 1978 modifiée, vous bénéficiez d'un droit d'accès et de rectification aux informations qui vous concernent. Sui vous souhaitez exercer ce droit et obtenir communication des informations vous concernant, contacter Antony Affilé à sunfrisbee@gmail.com.
      </p>
    </div>;
  }
}

DetailsPanel.propTypes = {
  model: React.PropTypes.shape({
    firstName: React.PropTypes.string.isRequired,
    lastName: React.PropTypes.string.isRequired
  }),
  onSubmit: React.PropTypes.func.isRequired
};

export default DetailsPanel;
