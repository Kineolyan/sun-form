import React from 'react';
import _ from 'lodash';

import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import Paper from 'material-ui/Paper';

class JeunePanel extends React.Component {
  constructor(props) {
    super(props);
    this.valueKeys = new Set();

    const model = this.props.model;
    model.lastName = model.lastName || this.props.details.lastName;

    this.state = model;
  }

  get model() {
    return this.state;
  }

  setModel(key, event, value) {
    this.valueKeys.add(key);
    this.setState({[key]: value});
  }

  submit() {
    const newModel = _.pick(this.state, Array.from(this.valueKeys.values()));
    this.props.onSubmit(newModel);
  }

  render() {
    return <div>
      <p>Jeune joueur, besoin {"d'un"} accord parental</p>
      <div>
        Nom du responsable<br/>
        <TextField hintText="Prénom"
          defaultValue={this.model.firstName}
          onChange={this.setModel.bind(this, 'firstName')} />
        <TextField hintText="Nom"
          defaultValue={this.model.lastName}
          onChange={this.setModel.bind(this, 'lastName')} />
      </div>
      <div>
        <TextField hintText="Téléphone"
          defaultValue={this.model.phoneNumber}
          onChange={this.setModel.bind(this, 'phoneNumber')} />
      </div>
      <Paper>
        En continuant l'adhésion, vous donnez au Sun l'autorisation suivante:
        <p style={{textStyle: 'italic'}}>
          ￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼￼Je soussigné(e) {this.model.firstName} {this.model.lastName}, responsable légal de
          l'enfant {this.props.details.firstName} {this.props.details.lastName}, autorise les responsables ou
          entraîneurs de la section sportive à prendre toutes les dispositions nécessaires en cas
          d'incident ou d'accident.
        </p>
      </Paper>
      <div>
        <RaisedButton label="Continuer" primary={true} style={{margin: 12}}
          onClick={this.submit.bind(this)}/>
      </div>
    </div>;
  }
}

JeunePanel.propTypes = {
  details: React.PropTypes.object,
  model: React.PropTypes.object,
  onSubmit: React.PropTypes.func.isRequired
}

export default JeunePanel;
