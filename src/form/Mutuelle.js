import React from 'react';
import _ from 'lodash';

import MUTUELLES from '../data/Mutuelles.js';
import JoueurReader from '../data/JoueurReader';

import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';
import RaisedButton from 'material-ui/RaisedButton';
import Paper from 'material-ui/Paper';
import IconButton from 'material-ui/IconButton';
import ContentBackspace from 'material-ui/svg-icons/content/backspace';
import Chip from 'material-ui/Chip';

const DEFAULT_MUTUELLE = _.findKey(MUTUELLES, {default: true});
class MutuellePanel extends React.Component {
  constructor(props) {
    super(props);

    const mutuelle = MUTUELLES[this.props.mutuelle];
    this.state = {
      mutuelle: this.props.mutuelle,
      showChoice: !mutuelle.default
    };
  }

  get mutuelle() {
    return MUTUELLES[this.state.mutuelle];
  }

  setMutuelle(event, mutuelle) {
    this.setState({mutuelle});
  }

  submit() {
    this.props.onSubmit(this.state.mutuelle);
  }

  renderCost() {
    if (this.props.licence !== undefined) {
      const cout = JoueurReader.computeCost({
        mutuelle: this.state.mutuelle,
        license: this.props.licence
      });

      const coutMutuelle = MUTUELLES[this.state.mutuelle].cout;
      let diff = null;
      if (coutMutuelle !== 0) {
        diff = ` (${coutMutuelle > 0 ? '+' : '-'}${JoueurReader.formatCost(Math.abs(coutMutuelle))})`;
      }
      return <div>
        Coût mis-à-jour de la licence: <Chip style={{display: 'inline-block'}}>{JoueurReader.formatCost(cout)}</Chip>
      <span className="notice">{diff}</span>
      </div>;
    } else {
      return null;
    }
  }

  textToDom(value) {
    const parts = value.split('\n');
    return parts.map((part, i)  => <span key={i}>{i > 0 ? <br/> : null}{part}</span>);
  }

  renderChoice() {
    const chosenMutuelle = MUTUELLES[this.state.mutuelle];

    return <div>
      {this.renderCost()}
      <RadioButtonGroup name="mutuelle" valueSelected={this.state.mutuelle}
          onChange={this.setMutuelle.bind(this)}>
          {
            _(MUTUELLES)
              .map((mut, key) => ({key, order: mut.cout}))
              .sortBy('order')
              .map(({key}) => {
                const mutuelle = MUTUELLES[key];
                return <RadioButton key={key}
                  value={key} label={mutuelle.name}/>;
              })
              .value()
          }
      </RadioButtonGroup>
      <div>
        Choisir de nouveau la mutuelle par défaut <IconButton onTouchTap={() => this.setMutuelle(null, DEFAULT_MUTUELLE)}>
          <ContentBackspace style={{height: 16}}/>
        </IconButton>
      </div>
      <Paper style={{marginTop: 10, padding: 10}}>
        <span style={{fontStyle: 'italic', fontWeight: 'bold'}}>Description</span> : {this.textToDom(chosenMutuelle.description)}
      </Paper>
    </div>;
  }

  render() {
    let choices;
    if (this.state.showChoice) {
      choices = <div>
        {this.renderChoice()}
        <RaisedButton label="Continuer" primary={true} style={{margin: 12}}
          onClick={this.submit.bind(this)}/>
      </div>;
    } else {
      choices = <div>
        <RaisedButton label="Continuer" primary={true} style={{margin: 12}}
          onClick={this.submit.bind(this)}/>
        <RaisedButton label="Choisir une autre mutuelle"
          style={{margin: 12}}
          onClick={() => this.setState({showChoice: true})}/>
      </div>;
    }
    return <div>
      <p>
        Le Sun Frisbee Club de Créteil recommande à ses licenciés de prendre une mutuelle garantissant la responsabilité civile et l'indemnisation des dommages coporels. Celle-ci est incluse dans le coût de la licence.<br/>
      Toutefois, si vous possédez déjà ces garanties ou souhaitez ajouter des compléments à votre license, clicquez sur le bouton "Choisir une autre mutuelle".
      </p>
      {choices}
    </div>;
  }
}

MutuellePanel.propTypes = {
  licence: React.PropTypes.object,
  mutuelle: React.PropTypes.string,
  onSubmit: React.PropTypes.func.isRequired
}

MutuellePanel.defaultProps = {
  mutuelle: DEFAULT_MUTUELLE
}

export default MutuellePanel;
