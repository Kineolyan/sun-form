import React, {Component} from 'react';

import LICENSES from '../data/Licences';
import JoueurReader from '../data/JoueurReader';

import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';
import RaisedButton from 'material-ui/RaisedButton';
import Paper from 'material-ui/Paper';
import Toggle from 'material-ui/Toggle';
import Chip from 'material-ui/Chip';

const STYLES = {
  block: {
    maxWidth: 250,
  },
  radioButton: {
    marginBottom: 16,
  },
};

class LicensePanel extends Component {
  constructor(props) {
    super(props);

    this.state = {
      type: this.props.license.type,
      filterByAge: true
    };
  }

  selectType(event, value) {
    this.setState({type: value});
  }

  addDisc(event, value) {
    this.setState({withDisc: value});
  }

  submit() {
    this.props.onSubmit({
      type: this.state.type,
      withDisc: this.state.withDisc === true
    });
  }

  renderHeader() {
    if (this.props.details) {
      return <Paper className="player-header">
        Joueur: {this.props.details.firstName} {this.props.details.lastName}
      </Paper>;
    } else {
      return null;
    }
  }

  renderLicense(license) {
    const label = `${license.name} - ${JoueurReader.formatCost(license.cout)}`;
    return <RadioButton key={license.name}
      value={license.name} label={label}
      style={STYLES.radioButton} />
  }

  render() {
    const birthDate = this.props.details.birthDate;
    let licenses;
    if (birthDate && this.state.filterByAge) {
      const d = new Date(birthDate);
      licenses = LICENSES.filter(l => l.dateRange(d));
    } else {
      licenses = LICENSES;
    }

    let cost = '--';
    if (this.state.type) {
      cost = JoueurReader.computeCost({ license: this.state });
    }
    return (
      <div>
        {this.renderHeader()}
        <RadioButtonGroup name="player-type" defaultSelected={this.state.type}
          onChange={this.selectType.bind(this)}>
          {licenses.map(this.renderLicense)}
        </RadioButtonGroup>
        <div>
          <Toggle
            label="J'achète un disque du club avec mon inscription 10 € (au lieu de 12 €)"
            defaultToggled={this.props.license.withDisc}
            labelPosition="right"
            onToggle={this.addDisc.bind(this)} />
        </div>
        <div style={{padding: '10px 10px 5px 20px'}}>
          Coût de la licence : <Chip style={{display: 'inline-block'}}>{JoueurReader.formatCost(cost)}</Chip>
        </div>
        <div>
          <RaisedButton label="Continuer" primary={true} style={{margin: 12}}
            onClick={this.submit.bind(this)}/>
        </div>
        <p className="notice">
          Les types de license proposés dépendent de l'âge que vous avez renseigné dans les détails.<br/>
          Pas de type valide pour votre âge: cliquez-ici
          pour <i onClick={() => this.setState({filterByAge: false})}>tout afficher</i>.
        </p>
      </div>
    );
  }
}

LicensePanel.propTypes = {
  details: React.PropTypes.object.isRequired,
  license: React.PropTypes.object,
  onSubmit: React.PropTypes.func.isRequired
};

export default LicensePanel;
