import _ from 'lodash';

import LICENSES from './Licences';
import MUTUELLES from './Mutuelles';

function calculateAge(birthday) {
    var ageDifMs = Date.now() - new Date(birthday);
    var ageDate = new Date(ageDifMs); // miliseconds from epoch
    return Math.abs(ageDate.getUTCFullYear() - 1970);
}

const JoueurReader = {
  isMale(details) {
    return details.sexe === 'homme';
  },
  getAge(details) {
    return calculateAge(details.birthDate);
  },
  isYoung(details) {
    return this.getAge(details) < 18;
  },
  computeCost({license, mutuelle}) {
    const chosenLicence = _.find(LICENSES, {name: license.type});
    const chosenMutuelle = MUTUELLES[mutuelle];

    let cost = license.withDisc ? 1000 : 0;
    if (chosenLicence !== undefined) { cost += chosenLicence.cout; }
    if (chosenMutuelle !== undefined) { cost += chosenMutuelle.cout; }
    return cost;
  },
  formatCost(value) {
    const matches = /^(\d+)(\d{2})$/.exec(value);
    return `${matches[1]}.${matches[2]} €`;
  }
}

export default JoueurReader;
