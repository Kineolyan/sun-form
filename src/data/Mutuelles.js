const MUTUELLES = {
  RC: {
    name: 'Responsabilité civile',
    description: `Responsabilité civile uniquement.
      En choisissant cette option de mutuelle, le licencié ne bénéficiera d'aucune indemnité au titre des dommages corporels dont il pourrait être victime à l'occasion des actvités mises en place par la la FFDF et les structures qui lui sont affiliées.`,
    cout: -100
  },
  'RC-IDC': {
    name: '￼Responsabilité civile + Indemnisation des Dommages Corporels',
    description: `Option de mutuelle recommandée
      Elle offre une couverture en cas de dommages causés par le licencié dans la pratique du sport et à l'ocassion des activités mises en place par la FFDF et les structures qui lui sont liés`,
    cout: 0,
    default: true
  },
  GC: {
    name: 'Garantie complémentaire',
    description: `Extension de la garantie «Indemnisation des Dommages corporels»
      Un détail des garanties étendues peut être obtenue sur demande.`,
    cout: 1165
  }
};

export default MUTUELLES;
