const CURRENT_YEAR = 2017;
function getYear(date) {
  return date.getFullYear();
}
const A18 = CURRENT_YEAR  - 18;
const A21 = CURRENT_YEAR - 21;
const A16 = CURRENT_YEAR - 16;
const A14 = CURRENT_YEAR - 14;
const A12 = CURRENT_YEAR - 12;

export default [
  {
    name: 'Joueur',
    cout: 8000,
    dateCaption: `<= ${A18}`,
    dateRange: date => getYear(date) <= A18
  },
  {
    name: 'Joueur U20',
    cout: 5000,
    dateCaption: `${A18} - ${A21 - 1}`,
    dateRange: date => {
      const year = getYear(date);
      return A21 < year && year <= A18;
    }
  },
  {
    name: 'Joueur U17',
    cout: 5000,
    dateCaption: `${A16} - ${A18 - 1}`,
    dateRange: date => {
      const year = getYear(date);
      return A18 < year && year <= A16;
    }
  },
  {
    name: 'Joueur U15',
    cout: 4500,
    dateCaption: `${A14} - ${A16 - 1}`,
    dateRange: date => {
      const year = getYear(date);
      return A16 < year && year <= A14;
    }
  },
  {
    name: 'Joueur U13',
    cout: 4500,
    dateCaption: `${A12} - ${A14 - 1}`,
    dateRange: date => {
      const year = getYear(date);
      return A14 < year && year <= A12;
    }
  },
  {
    name: 'Étudiant',
    cout: 3000,
    dateRange: () => true,
    dateCaption: 'Toute année'
  },
  {
    name: 'Loisir',
    cout: 4500,
    dateRange: () => true,
    dateCaption: 'Toute année'
  }
];
